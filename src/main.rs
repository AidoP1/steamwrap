extern crate steamworks;

use steamworks::{
    Client,
    SendType,
    SingleClient,
    SteamId,
    P2PSessionRequest,
};

use std::{
    thread,
    sync::mpsc,
    net::{
        TcpListener,
        TcpStream
    },
    io::{
        Read,
        Write,
        ErrorKind
    },
    time::Duration
};

const SLEEP_TIME: Duration = Duration::from_nanos(10);

fn main() {
    let (client, mut handler) = Client::init().unwrap();

    let mut args = std::env::args().skip(1);

    let listen_port = args.next().expect("Need port to bind to");

    let steamid = SteamId::from_raw(args.next().expect("Need SteamID of host").parse().expect("ID must be an integer"));
    
    print!("    Waiting for {}", steamid.raw());
    let mut tick = true;
    loop {
        if tick { print!("\r x\x1b[34Cx ") } else { print!("\r o\x1b[34Co ") };
        let mut buffer = [0; 1];
        if let Some(_) = client.networking().read_p2p_packet(&mut buffer) {
            break;
        };
        client.networking().send_p2p_packet(steamid, SendType::Reliable, b"Connecting...");

        let _ = std::io::stdout().flush();
        tick = !tick;

        std::thread::sleep(std::time::Duration::from_secs(1));
    }
    println!();
    //let steamid = SteamId::from_raw(buffer[..8].iter().fold(0, |byte, &total| byte << 8 | total as u64));

    let open_connection = |mut connection: TcpStream, handler: SingleClient| {
        connection.set_nonblocking(true).expect("Failed to create non-blocking stream");

        let (inbound_data_tx, inbound_data_rx) = mpsc::sync_channel(8);
        let (outbound_data_tx, outbound_data_rx) = mpsc::sync_channel(8);
        
        let (inbound_close_tx, inbound_close_rx) = mpsc::channel();
        let (outbound_close_tx, outbound_close_rx) = mpsc::channel();

        // Inbound
        thread::spawn({let client = client.clone(); move || {
            let mut buffer = [0;  2048];
            let mut connection_open = true;
            while connection_open {
                if let Some((steamid, len)) = client.networking().read_p2p_packet(&mut buffer) {
                    println!("Received {}B from {}", len, steamid.raw());
                    if let Err(error) = inbound_data_tx.send((steamid, buffer, len)) {
                        eprintln!("Failed to send data: {}", error)
                    };
                }
                if let Ok(_) = inbound_close_rx.try_recv() {
                    connection_open = false;
                }

                // Give back resources for a minute
                thread::sleep(SLEEP_TIME);
            }
        }});

        let networking = thread::spawn(move || {
            let mut buffer = [0; 2048];
            loop {
                handler.run_callbacks();

                if let Ok((_, data, len)) = inbound_data_rx.try_recv() {
                    match connection.write(&data[..len]) {
                        Ok(0) => { eprintln!("Connection closed"); return handler },
                        Ok(n) => {
                            println!("Sent {}B locally", n);
                            if n < len {
                            eprintln!("Malformed packet sent!")
                        }},
                        Err(ref error) if error.kind() == ErrorKind::WouldBlock => (),
                        Err(error) => { eprintln!("Failed to write data to socket: {}", error); return handler }
                    }
                }

                match connection.read(&mut buffer) {
                    Ok(0)   => { eprintln!("Connection closed"); return handler },
                    Ok(len) => {
                        println!("Received {}B locally", len);
                        if let Err(error) = outbound_data_tx.send((buffer,  len)) {
                            eprintln!("Failed to forward data: {}", error)
                        }
                    },
                    Err(ref error) if error.kind() == ErrorKind::WouldBlock => (),
                    Err(error) => { eprintln!("Failed to read data from socket: {}", error); return handler }
                }

                // Give back resources for a minute
                thread::sleep(SLEEP_TIME);
            }
        });

        // Outbound
        thread::spawn({let client = client.clone(); move || {
            let mut connection_open = true;
            while connection_open {
                if let Ok((data, len)) = outbound_data_rx.try_recv() {
                    println!("Sending {}B to {}", len, steamid.raw());
                    client.networking().send_p2p_packet(steamid, SendType::Reliable, &data[..len]);
                }
                if let Ok(_) = outbound_close_rx.try_recv() {
                    connection_open = false;
                }

                // Give back resources for a minute
                thread::sleep(SLEEP_TIME);
            }
        }});

        let handler = networking.join().unwrap();
        inbound_close_tx.send(()).expect("Failed to close connection");
        outbound_close_tx.send(()).expect("Failed to close connection");
        return handler;
    };

    // Allow incoming steam packets
    client.register_callback({ let client = client.clone(); move |callback:  P2PSessionRequest| {
        println!("{} wants to start a session", callback.remote.raw());
        client.networking().accept_p2p_session(callback.remote);
    }});

    let target = args.next();

    // Wait for connections
    if let Some(target) = target {
        println!("Started server tunnel");

        // Optional address for server
        while let Ok(connection) = TcpStream::connect(format!("{}:{}", target, listen_port)) {
            if let Err(_) = connection.set_nodelay(true) { eprintln!("Failed to disable buffering, this may create invalid packets") };
            if let Err(_) = connection.set_read_timeout(None) { eprintln!("Failed to disable read timeout, this may cause the connection to drop") };
            if let Err(_) = connection.set_write_timeout(None) { eprintln!("Failed to disable write timeout, this may cause the connection to drop") };
            handler = open_connection(connection, handler)
        }
        eprintln!("Unable to connect to {}:{}", target, listen_port);
    } else {
        let listener = TcpListener::bind(format!("127.0.0.1:{}", listen_port)).expect(format!("Unable to bind to '127.0.0.1:{}'", listen_port).as_ref());
        println!("Started client tunnel");

        for connection in listener.incoming() {
            let connection = connection.expect("Unable to open connection");
            if let Err(_) = connection.set_nodelay(true) { eprintln!("Failed to disable buffering, this may create invalid packets") };
            if let Err(_) = connection.set_read_timeout(None) { eprintln!("Failed to disable read timeout, this may cause the connection to drop") };
            if let Err(_) = connection.set_write_timeout(None) { eprintln!("Failed to disable write timeout, this may cause the connection to drop") };
            handler = open_connection(connection, handler)
        }
    }
}
